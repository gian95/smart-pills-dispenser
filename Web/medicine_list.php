<?php
include "./logged.php";
include "./connect.php";

$query = "SELECT f.ID, f.Nome 
                FROM Farmaco AS f
                JOIN Assunzione AS a
                ON f.ID = a.Farmaco
                JOIN Utente AS u
                ON a.Utente = u.ID 
                WHERE u.ID = $user[2]
                ORDER BY f.Nome ASC";
$result = mysqli_query($connessione, $query);
if ($result) {
    $array = array();
    while ($row = mysqli_fetch_array($result, MYSQLI_NUM)) {
        array_push($array, [$row[0], $row[1]]);
    }
}
echo json_encode($array);

mysqli_close($connessione);
