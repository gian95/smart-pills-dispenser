<?php include "./logged.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SmartPillbox</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>


<body id="page-top">

    <div id="wrapper">
        <?php include "./sidebar.html"; ?>
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <?php include "./navbar.php" ?>
                <div class="container-fluid">

                    <h1 class="h3 mb-2 text-gray-800">Aggiungi nuovo farmaco</h1>
                    <p class="mb-4">Indica il nome del farmaco che intendi aggiungere e il cassetto nel quale verrà inserito.</p>

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="p-4">
                                        <?php

                                        include "./connect.php";

                                        if (!empty($_POST['medicina'])) {

                                            foreach ($_POST['medicina'] as $key => $medicina) {
                                                if ($medicina != "") {

                                                    $sql = "SELECT ID FROM `Farmaco` WHERE Nome='$medicina'";
                                                    $res = mysqli_query($connessione, $sql);
                                                    $exist = mysqli_num_rows($res);

                                                    if ($exist) {
                                                        $id_med = mysqli_fetch_row($res);
                                                        $query1 = "INSERT INTO `Assunzione`(`Utente`, `Farmaco`) VALUES ('$user[2]', '$id_med[0]')";
                                                        $ris = mysqli_query($connessione, $query1);
                                                    } else {
                                                        $query = "INSERT INTO `Farmaco`(`Nome`) VALUES ('$medicina')";
                                                        $result = mysqli_query($connessione, $query);
                                                        $id_medicine = mysqli_insert_id($connessione);
                                                        $query1 = "INSERT INTO `Assunzione`(`Utente`, `Farmaco`) VALUES ('$user[2]', '$id_medicine')";
                                                        $ris = mysqli_query($connessione, $query1);
                                                        //if ($ris === TRUE) { echo "OK"; }
                                                        //else { echo "Error: " . $query1 . "<br />" . $connessione->error; }
                                                    }
                                                }
                                            }
                                        }

                                        if ($ris === TRUE) {
                                        ?>
                                            <form role="form">
                                                <div class="text-center">
                                                    <h1 class="h3 mb-2 text-gray-800">Farmaco registrato</h1>
                                                    <p class="mb-4">La procedura di registrazione del farmaco si è conclusa correttamente. Premi il pulsante di seguito per registrarne altri.</p>
                                                    <br>
                                                    <a href="new_medicine.php" class="btn btn-primary btn-user">Aggiungi nuovo farmaco</a>
                                                </div>
                                            </form>
                                        <?php
                                        } else {
                                        ?>
                                            <form role="form">
                                                <div class="text-center">
                                                    <h1 class="h3 mb-2 text-gray-800">Farmaco non registrato</h1>
                                                    <p class="mb-4">La procedura di registrazione del farmaco non si è conclusa correttamente. Premi il pulsante di seguito per ripetere la procedura.</p>
                                                    <br>
                                                    <a href="new_medicine.php" class="btn btn-primary btn-user">Riprova</a>
                                                </div>
                                            </form>
                                        <?php
                                        }
                                        mysqli_close($connessione);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include "./footer.html" ?>
        </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include "./logout.php" ?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>

    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>