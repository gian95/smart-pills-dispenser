<?php include "./logged.php";?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SmartPillbox</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body id="page-top">

<div id="wrapper">
  <?php include "./sidebar.html";?> 
    <div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">
    <?php include "./navbar.php" ?>


    <!-- Delete Modal-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminare definitivamente?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Se elimini questo promemoria in maniera definitiva non avrai più modo di recuperarlo in seguito. Vuoi procedere?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Annulla</button>
                    <a onclick="deleteFile()" class="btn btn-danger">Elimina</a>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
  function deleteFile(){
    var id= document.getElementById("memoid").value;
    console.log(id);
      $.ajax({
          type:'POST',
          url:'delete_memo.php',
          data:'idmemo='+id,
          success:function(msg){
          info = JSON.parse(msg);
          console.log(info); 
          if (info.Esito=="Ok"){
            location.reload();
          }           
        }
      });
  }
  </script>

    <?php 
                            
    include "./connect.php";

    $sql="SELECT Nome, Data_partenza FROM Piano_terapeutico WHERE Utente='$user[2]'";
    $result = mysqli_query($connessione, $sql);
    $exist= mysqli_num_rows($result);

    if($exist){

        $schedule_info=mysqli_fetch_row($result);
        $date= explode("-", $schedule_info[1]);
        $data= $date[2]."-". $date[1]."-".$date[0];
 
    ?>
        <div class="container-fluid">

        <h1 class="h3 mb-2 text-gray-800">La tua programmazione mensile</h1>
        <p class="mb-4">L'elenco completo delle medicine e dei rispettivi giorni ed orari di assunzione.</p>
        <p class="text-gray-800 text-right">Inizio programmazione mensile:<b> <?php echo "$data";?></b>&ensp;<a href="modify_schedule.php">Modifica <i class="fas fa-sm fa-pencil-alt"></i></a></p>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo "$schedule_info[0]";?></h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Settimana</th>
                                <th>Giorno</th>
                                <th>Ora</th>
                                <th>Medicina</th>
                                <th>Modifica</th>
                                <th>Elimina</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Settimana</th>
                                <th>Giorno</th>
                                <th>Ora</th>
                                <th>Medicina</th>
                                <th>Modifica</th>
                                <th>Elimina</th>
                            </tr>
                        </tfoot>
                        <tbody id="rowTable">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        $(document).ready(function() {
        $('#dataTable').DataTable( {
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Righe da _START_ a _END_ su _TOTAL_ totali",
                "infoEmpty": "Nessun elemento presente",
                "infoFiltered": "(su _MAX_ risultati totali)",
                "paginate": {
                    "first":      "Prima",
                    "last":       "Ultima",
                    "next":       "Successiva",
                    "previous":   "Precedente"
                },
                "search":         "Cerca:",
            }
            } );
        } );
        $.ajax({
            type:'POST',
            url:'memo_list.php',
            success:function(msg){
            info = JSON.parse(msg);
            console.log(info);               
            

            for(var i=0; i<info.length;i++){
                var line = document.createElement("TR");

                var a = document.createElement("TD");
                var week = document.createTextNode(info[i][0]);
                a.appendChild(week);
                line.appendChild(a);
                var b = document.createElement("TD");
                var day = document.createTextNode(info[i][1]);
                b.appendChild(day);
                line.appendChild(b);
                var c = document.createElement("TD");
                var hour = document.createTextNode(info[i][2].slice(0, -3));
                c.appendChild(hour);
                line.appendChild(c);
                var d = document.createElement("TD");
                var medicine = document.createTextNode(info[i][3]);
                d.appendChild(medicine);
                line.appendChild(d);
                var e = document.createElement("TD");
                var link = document.createElement("a");
                link.innerHTML= '<i class="fas fa-pencil-alt"></i>';
                var text_link = document.createTextNode(link);
                link.setAttribute('href', "modify_memo.php"+"#"+info[i][4]);
                link.appendChild(text_link);
                e.appendChild(link);
                line.appendChild(e);
                var f = document.createElement("TD");
                var link = document.createElement("a");
                link.innerHTML= '<i class="fas fa-trash-alt"></i>';
                var text_link = document.createTextNode(link);
                link.setAttribute('data-toggle', "modal");
                link.setAttribute('href', "#deleteModal");
                link.setAttribute('onclick', "document.getElementById('memoid').value ="+info[i][4]);
                link.appendChild(text_link);
                f.appendChild(link);
                line.appendChild(f);
                
                document.getElementById('rowTable').appendChild(line);
            }
            }
         }); 
    </script>
 <div class="input-group"><input type="hidden" class="form-control" name="memoid" id="memoid" value="" /></div>

<?php 
 }
else{
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-8">
                
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary text-center">Crea il tuo piano terapeutico</h6>
                    </div>
                    <div class="card-body">
                    <div class="text-center"><b> Benvenuto <?php echo "$user[0] $user[1]";?>!</b></div><br>Per iniziare crea il tuo piano terapeutico e dagli il nome che preferisi. 
                        Successivamente ad esso potrai aggiungere tutti i promemoria per i farmaci di cui hai bisogno, modificarli o eliminarli in qualsiasi momento.
                        Il tuo sPillBox si sincronizzerà automaticamente con le ultime modifiche che hai apportato.<br><br>
                        <div class="text-center"><a role="button" href="new_schedule.php" class="btn btn-primary btn-user">Crea nuovo piano</a></div>
                    </div>
                </div>

            </div>
            <div class="col-lg-2">
            </div>
        </div>
    </div>

<?php
  }
 ?>

 <div class="container-fluid">

            <div class="row">
                <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div
                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Dropdown Header:</div>
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-area">
                            <canvas id="myAreaChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div
                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Revenue Sources</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Dropdown Header:</div>
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-pie pt-4 pb-2">
                            <canvas id="myPieChart"></canvas>
                        </div>
                        <div class="mt-4 text-center small">
                            <span class="mr-2">
                                <i class="fas fa-circle text-primary"></i> Direct
                            </span>
                            <span class="mr-2">
                                <i class="fas fa-circle text-success"></i> Social
                            </span>
                            <span class="mr-2">
                                <i class="fas fa-circle text-info"></i> Referral
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
 </div>
<?php include "./footer.html" ?>

</div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <?php include "./logout.php" ?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>

    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>
    
</body>

</html>