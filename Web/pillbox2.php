<?php include "./logged.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SmartPillbox</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>


<body id="page-top">

    <div id="wrapper">
        <?php include "./sidebar.html"; ?>
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <?php include "./navbar.php" ?>
                <div class="container-fluid">

                    <h1 class="h3 mb-2 text-gray-800">Organizza sPillBox</h1>
                    <p class="mb-4">Scegli il farmaco dall'elenco e il corrispondente cassetto dello sPillBox nel quale lo inserirai.</p>

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="p-4">
                                        <?php

                                        include "./connect.php";

                                        $sql = "SELECT MAC FROM Smart_pillbox WHERE Utente='$user[2]'";
                                        $result = mysqli_query($connessione, $sql);
                                        $mac = mysqli_fetch_row($result);

                                        if (!empty($_POST['medicine'])) {
                                            foreach ($_POST['medicine'] as $key => $medicina) {
                                                if ($medicina != "") {

                                                    $box = empty($_POST['box'][$key]) ? NULL : $_POST['box'][$key];

                                                    $sql1 = "SELECT * FROM Cassetto WHERE Numero='$box' AND Smart_pillbox='$mac[0]'";
                                                    $resul = mysqli_query($connessione, $sql1);
                                                    $exist = mysqli_num_rows($resul);

                                                    if ($exist) {
                                                        $query1 = " UPDATE `Cassetto` SET Farmaco='$medicina' WHERE Numero='$box' AND Smart_pillbox='$mac[0]'";
                                                        $ris = mysqli_query($connessione, $query1);
                                                    } else {
                                                        $query1 = "INSERT INTO `Cassetto`(`Numero`,`Smart_pillbox`,`Farmaco`) VALUES ('$box','$mac[0]','$medicina')";
                                                        $ris = mysqli_query($connessione, $query1);
                                                    }
                                                }
                                            }
                                        }

                                        if ($ris === TRUE) {
                                            date_default_timezone_set('Europe/Rome');
                                            $timestamp = date('Y-m-d H:i:s');
                                            $query2 = "UPDATE `Smart_pillbox` SET `Last_update`='$timestamp' WHERE Utente='$user[2]'";
                                            $res = mysqli_query($connessione, $query2);
                                            if ($res === TRUE) {
                                        ?>
                                                <form role="form">
                                                    <div class="text-center">
                                                        <h1 class="h3 mb-2 text-gray-800">Informazioni registrate</h1>
                                                        <p class="mb-4">La procedura di registrazione si è conclusa correttamente. Premi il pulsante di seguito per una nuova associazione.</p>
                                                        <br>
                                                        <a href="pillbox.php" class="btn btn-primary btn-user">Nuova associazione</a>
                                                    </div>
                                                </form>
                                            <?php
                                            }
                                        } else {
                                            ?>
                                            <form role="form">
                                                <div class="text-center">
                                                    <h1 class="h3 mb-2 text-gray-800">Informazioni non registrate</h1>
                                                    <p class="mb-4">Il farmaco selezionto è già stato inserito in un altro cassetto del tuo smart pillbox. Organizza il contenuto e poi premi il pulsante di seguito per ripetere la procedura.</p>
                                                    <br>
                                                    <a href="pillbox.php" class="btn btn-primary btn-user">Riprova</a>
                                                </div>
                                            </form>
                                        <?php
                                        }
                                        mysqli_close($connessione);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include "./footer.html" ?>
        </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include "./logout.php" ?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>

    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>