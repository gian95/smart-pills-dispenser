<?php include "./logged.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SmartPillbox</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">

    <div id="wrapper">
        <?php include "./sidebar.html"; ?>
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <?php include "./navbar.php" ?>

                <!-- Confirmation Modal-->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Eliminare tutti i promemoria?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">Nella tua programmazione sono presenti dei promemoria per questo farmaco, eliminando il farmaco eliminerai anche tutti i promemoria. Vuoi procedere? </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annulla</button>
                                <a onclick="document.getElementById('myForm').submit();" class="btn btn-danger">Elimina</a>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    function VerifyExistMemo() {
                        var medicine = document.getElementById('farmaco').value;
                        $.ajax({
                            type: 'POST',
                            url: 'exist_memo.php',
                            data: 'medicine=' + medicine,
                            success: function(msg) {
                                info = JSON.parse(msg);
                                console.log(info);
                                if (info.Exist == "no") {
                                    document.getElementById('myForm').submit();
                                } else {
                                    $('#myModal').modal('show');
                                }
                            }
                        });
                    }
                </script>

                <div class="container-fluid">

                    <h1 class="h3 mb-2 text-gray-800">Elimina farmaco</h1>
                    <p class="mb-4">Seleziona il nome del farmaco che intendi eliminare dalla tua lista.</p>

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="p-4">
                                        <form role="form" id="myForm" action="delete_medicine2.php" method="post">

                                            <?php
                                            include "./connect.php";

                                            $query = "SELECT f.ID, f.Nome 
                                                        FROM Farmaco AS f
                                                        JOIN Assunzione AS a
                                                        ON f.ID = a.Farmaco
                                                        JOIN Utente AS u
                                                        ON a.Utente = u.ID 
                                                        WHERE u.ID = $user[2]
                                                        ORDER BY f.Nome ASC";
                                            $result = mysqli_query($connessione, $query);
                                            ?>

                                            <div class="form-group row">
                                                <div class="col-sm-4 mb-3 mb-sm-0">
                                                    <b><label for="farmaco">Nome farmaco</label></b>
                                                    <select id="farmaco" name="farmaco" class="form-control" required>
                                                        <option value="">Seleziona il farmaco</option>
                                                        <?php
                                                        if (mysqli_num_rows($result) > 0) {
                                                            while ($row = mysqli_fetch_assoc($result)) {
                                                                echo '<option value="' . $row['ID'] . '">' . $row['Nome'] . '</option>';
                                                            }
                                                        } else {
                                                            echo '<option value=" ">Nessun farmaco presente</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div><br><br><br><br><br><br><br><br><br>
                                                <button type="button" onclick="VerifyExistMemo()" class="btn btn-primary btn-user btn-block">Elimina</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "./footer.html" ?>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include "./logout.php" ?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>

    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>