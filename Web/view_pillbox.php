<?php include "./logged.php";?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SmartPillbox</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body id="page-top">

<div id="wrapper">
  <?php include "./sidebar.html";?> 
    <div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">
    <?php include "./navbar.php" ?>


    <!-- Delete Modal-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminare definitivamente?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Se elimini questo elemento in maniera definitiva non avrai più modo di recuperarlo in seguito. Vuoi procedere?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Annulla</button>
                    <a onclick="deleteItem()" class="btn btn-danger">Elimina</a>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
  function deleteItem(){
    var id= document.getElementById("idbox").value;
    console.log(id);
      $.ajax({
          type:'POST',
          url:'delete_box.php',
          data:'idbox='+id,
          success:function(msg){
          info = JSON.parse(msg);
          console.log(info); 
          if (info.Esito=="Ok"){
            location.reload();
          }           
        }
      });
  }
  </script>

    <div class="container-fluid">

        <h1 class="h3 mb-2 text-gray-800">La tua sPillBox</h1>
        <p class="mb-4">I farmaci contenuti all'interno dei cassetti della tua sPillBox.</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Contenuto sPillBox</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Cassetto</th>
                                <th>Medicina</th>
                                <th>Elimina</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Cassetto</th>
                                <th>Medicina</th>
                                <th>Elimina</th>
                            </tr>
                        </tfoot>
                        <tbody id="rowTable">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        $(document).ready(function() {
        $('#dataTable').DataTable( {
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento corrispondente",
                "info": "Righe da _START_ a _END_ su _TOTAL_ totali",
                "infoEmpty": "Nessun elemento presente",
                "infoFiltered": "(su _MAX_ risultati totali)",
                "paginate": {
                    "first":      "Prima",
                    "last":       "Ultima",
                    "next":       "Successiva",
                    "previous":   "Precedente"
                },
                "search":         "Cerca:",
            }
            } );
        } );
        $.ajax({
            type:'POST',
            url:'pillbox_content.php',
            success:function(msg){
            info = JSON.parse(msg);
            console.log(info);               
            

            for(var i=0; i<info.length;i++){
                var line = document.createElement("TR");

                var a = document.createElement("TD");
                var box = document.createTextNode(info[i][0]);
                a.appendChild(box);
                line.appendChild(a);
                var b = document.createElement("TD");
                var medicine = document.createTextNode(info[i][1]);
                b.appendChild(medicine);
                line.appendChild(b);
                var c = document.createElement("TD");
                var link = document.createElement("a");
                link.innerHTML= '<i class="fas fa-trash-alt"></i>';
                var text_link = document.createTextNode(link);
                link.setAttribute('data-toggle', "modal");
                link.setAttribute('href', "#deleteModal");
                link.setAttribute('onclick', "document.getElementById('idbox').value ="+info[i][0]);
                link.appendChild(text_link);
                c.appendChild(link);
                line.appendChild(c);
                
                document.getElementById('rowTable').appendChild(line);
            }
            }
         }); 
    </script>
 <div class="input-group"><input type="hidden" class="form-control" name="idbox" id="idbox" value="" /></div>

 </div>
<?php include "./footer.html" ?>

</div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <?php include "./logout.php" ?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>
    
</body>

</html>