<?php 
    include "./logged.php";
    include "./connect.php"; 

    $query = "SELECT c.Numero, f.Nome 
              FROM Cassetto AS c 
              JOIN Farmaco AS f
              ON c.Farmaco = f.ID
              JOIN Smart_pillbox AS s
              ON c.Smart_pillbox = s.MAC 
              WHERE s.Utente ='$user[2]' AND c.Farmaco IS NOT NULL ORDER BY c.Numero ASC"; 
    $result = mysqli_query($connessione,$query);
    if ($result) {
        $array=array();
        while ($row=mysqli_fetch_array($result, MYSQLI_NUM)){
            array_push($array,[$row[0],$row[1]]);
        }
    }
    echo json_encode($array);
    
    mysqli_close($connessione);
?>