<?php 
    include "./logged.php";
    include "./connect.php"; 

    $query = "SELECT s.Nome, g.Nome, p.Ora, f.Nome, p.ID
              FROM Promemoria AS p
              JOIN Piano_terapeutico AS pt
              ON p.Piano_terapeutico = pt.ID
              JOIN Utente AS u
              ON pt.Utente = u.ID
              JOIN Farmaco AS f
              ON p.Farmaco = f.ID
              JOIN Settimana AS s
              ON p.Settimana = s.Numero
              JOIN Giorno AS g
              ON p.Giorno = g.Numero
              WHERE u.ID = '$user[2]'
              ORDER BY s.Numero, g.Numero, p.Ora ASC"; 
    $result = mysqli_query($connessione,$query);
    if ($result) {
        $array=array();
        while ($row=mysqli_fetch_array($result, MYSQLI_NUM)){
            array_push($array,[$row[0],$row[1],$row[2],$row[3],$row[4]]);
        }
    }
    echo json_encode($array);
    
    mysqli_close($connessione);
?>