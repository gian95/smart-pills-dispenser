<?php include "./logged.php";?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SmartPillbox</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body id="page-top">

<div id="wrapper">
  <?php include "./sidebar.html";?> 
    <div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">
<?php include "./navbar.php" ?>
    <div class="container-fluid">

        <h1 class="h3 mb-2 text-gray-800">Aggiungi farmaco</h1>
        <p class="mb-4">Indica il nome del farmaco da inserire nel tuo elenco. Premi su "+ Aggiungi campo" per inserirne più di uno.</p>

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-4">
                                <form role="form"  action="new_medicine2.php" method="post">
                                    <b><label for="medicina">Nome farmaco</label></b>
                                    <div id="inputFormRow" class="form-group row"> 
                                        <div class="col-sm-4 mb-3 mb-sm-0">
                                        <input type="text" id="medicina" name="medicina[]" class="form-control" placeholder="Inserisci nome farmaco" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                            <div>                
                                                <button id="removeRow" type="button" class="btn btn-danger btn-user">Elimina</button>
                                            </div>
                                        </div>
                                    </div> 

                                    <div id="newRow"></div>
                                    <a id="addRow" href="#"><b> + Aggiungi campo<b></a>

                                    <script type="text/javascript">
                                            // add row
                                            $("#addRow").click(function () {
                                                var html = '';
                                                html += '<div id="inputFormRow" class="form-group row">';
                                                html += '<div class="col-sm-4 mb-3 mb-sm-0">';
                                                html += '<input type="text" id="medicina" name="medicina[]" class="form-control form-control-user" placeholder="Inserisci nome farmaco" autocomplete="off">';
                                                html += '</div>';
                                                html += '<div class="col-sm-2">';
                                                html += '<div><button id="removeRow" type="button" class="btn btn-danger btn-user">Elimina</button></div>';
                                                html += '</div></div>';

                                                $('#newRow').append(html);
                                            });

                                            // remove row
                                            $(document).on('click', '#removeRow', function () {
                                                $(this).closest('#inputFormRow').remove();
                                            });
                                            </script>
                                    <br><br><br><br>
                                    <button type="submit" class="btn btn-primary btn-user btn-block" name="submit">Salva</button>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        <?php include "./footer.html" ?>
    </div>
</div>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <?php include "./logout.php" ?>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>
    
</body>

</html>