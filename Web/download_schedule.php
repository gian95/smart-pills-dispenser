<?php 

    include "./connect.php";

    $foo = file_get_contents("php://input");
    $json=json_decode($foo, true);
    $MAC=$json["MAC"];
    $last_update_pillbox=$json["LastUpdate"];
    $day=$json["Day"];
    $time=$json["Time"];    
    $memo_list=array();
    
    $query = "SELECT Last_update FROM Smart_pillbox WHERE MAC='$MAC' "; 
    $resul = mysqli_query($connessione,$query);
    $last_update_schedule=mysqli_fetch_row($resul);

    //if lastupdate of the smartpillbox is older than the last modification of the schedule
    //download all starting from today until the end of month
    if(strcmp($last_update_pillbox,"none")==0 || strcmp($last_update_pillbox,$last_update_schedule[0])<0){
        
        $query1 = "SELECT p.Data_partenza
                   FROM Piano_terapeutico AS p
                   JOIN Utente AS u
                   ON p.Utente = u.ID
                   JOIN Smart_pillbox AS s
                   ON s.Utente = u.ID
                   WHERE s.MAC='$MAC' "; 
        $res = mysqli_query($connessione,$query1);
        $starting_date_schedule=mysqli_fetch_row($res);
        
        $origin = date_create($starting_date_schedule[0]);
        $target = date_create($day);
        $interval = date_diff($origin, $target);
        $day_difference= $interval->format('%r%a');
        $week = (($day_difference/7)%4)+1;    //current week of the schedule
        $current_week_day=date('w', strtotime($day)); //current day of the week 
        if($current_week_day==0){$current_week_day+=7;}
        $starting_week_day=date('w', strtotime($starting_date_schedule[0])); //day of the week for the starting date of the schedule
        if($starting_week_day==0){$starting_week_day+=7;}

        //count number of box for the smartpillox (for computing the degree of rotation)
        $query2 = "SELECT Numero FROM Cassetto WHERE Smart_pillbox='$MAC' "; 
        $ris = mysqli_query($connessione,$query2);
        $box_number=mysqli_num_rows($ris);

        //get all the element from the current week to the end of month once at a time
        $memo_ordered=array();
        for($k=$week; $k<=4; $k++){
            $query3 = "SELECT f.Nome, c.Numero, p.Settimana, p.Giorno, p.Ora   
                    FROM Farmaco AS f
                    JOIN Promemoria AS p
                    ON f.ID = p.Farmaco
                    JOIN Piano_terapeutico AS pt
                    ON p.Piano_terapeutico = pt.ID
                    JOIN Utente AS u
                    ON pt.Utente = u.ID
                    JOIN Smart_pillbox AS s
                    ON s.Utente = u.ID
                    JOIN Cassetto AS c
                    ON f.ID = c.Farmaco
                    WHERE s.MAC='$MAC' AND p.Settimana='$k' 
                    ORDER BY p.Settimana, p.Giorno, p.Ora ASC"; 
            $result = mysqli_query($connessione,$query3);
            if ($result) {
                $memo=array();
                while ($row=mysqli_fetch_array($result, MYSQLI_NUM)){
                    array_push($memo,[$row[0],$row[1],$row[2],$row[3],$row[4]]);
                }
            }
            //order the day of the week using the starting day of the schedule as the first of the week.
            for($i=0; $i<7; $i++){
                for($j=0; $j<sizeof($memo); $j++){
                    if($memo[$j][3]==(($i+$starting_week_day-1)%7)+1){
                        array_push($memo_ordered,[$memo[$j][0],$memo[$j][1],$memo[$j][2],$memo[$j][3],$memo[$j][4]]);
                    }
                }
            }
        }
        //remove the day of the current week before the current day
        $memo1=array();
        for($i=0; $i<sizeof($memo_ordered); $i++){
            if($memo_ordered[$i][2]==$week){
                if($current_week_day>=$starting_week_day && $current_week_day<=7){
                    if($memo_ordered[$i][3]>=$starting_week_day && $memo_ordered[$i][3]<=7 && $memo_ordered[$i][3]>=$current_week_day){
                        array_push($memo1,[$memo_ordered[$i][0],$memo_ordered[$i][1],$memo_ordered[$i][2],$memo_ordered[$i][3],$memo_ordered[$i][4]]);
                    }
                    else if($memo_ordered[$i][3]>=1 && $memo_ordered[$i][3]<$starting_week_day){
                        array_push($memo1,[$memo_ordered[$i][0],$memo_ordered[$i][1],$memo_ordered[$i][2],$memo_ordered[$i][3],$memo_ordered[$i][4]]);
                    }
                }
                else if($current_week_day>=1 && $current_week_day<$starting_week_day){
                    if($memo_ordered[$i][3]>=1 && $memo_ordered[$i][3]<$starting_week_day && $memo_ordered[$i][3]>=$current_week_day){
                        array_push($memo1,[$memo_ordered[$i][0],$memo_ordered[$i][1],$memo_ordered[$i][2],$memo_ordered[$i][3],$memo_ordered[$i][4]]);
                    }
                }
            }
            else{
                array_push($memo1,[$memo_ordered[$i][0],$memo_ordered[$i][1],$memo_ordered[$i][2],$memo_ordered[$i][3],$memo_ordered[$i][4]]);
            }
        }
        //remove the hour of the current day before the current hour
        $count=0;
        for($i=0; $i<sizeof($memo1); $i++){
            if(!($memo1[$i][2]==$week && $memo1[$i][3]==$current_week_day && $memo1[$i][4]<$time)){
                $count++;
                array_push($memo_list,[$memo1[$i][0],$memo1[$i][1],$memo1[$i][2],$memo1[$i][3],$memo1[$i][4]]);
            }
        }
        $status=0; //updated
        array_unshift($memo_list,[$count,$starting_week_day,$day_difference]); //unshift insert the element in the first position of the array
        array_unshift($memo_list,[$status,$box_number]);
        echo json_encode($memo_list);
    }
    else{
        $status=1; //uptodate
        array_push($memo_list,[$status]);
        echo json_encode($memo_list);
    }

    ob_start();
    var_dump($memo_list);
    $data = ob_get_clean();
    $fp = fopen("my-errors.log", "w");
    fwrite($fp, $data);
    fclose($fp);

    mysqli_close($connessione);
?>