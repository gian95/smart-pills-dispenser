<?php
include "./logged.php";
include "./connect.php";
$sql = "SELECT * FROM Piano_terapeutico WHERE Utente='$user[2]'";
$result = mysqli_query($connessione, $sql);
$exist = mysqli_num_rows($result);
if (!$exist) {
    header("Location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SmartPillbox</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body id="page-top">

    <div id="wrapper">
        <?php include "./sidebar.html"; ?>
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <?php include "./navbar.php" ?>
                <div class="container-fluid">

                    <h1 class="h3 mb-2 text-gray-800">Aggiungi promemoria farmaco</h1>
                    <p class="mb-4">Scegli il farmaco dall'elenco, seleziona il giorno e l'orario al quale va assunto.</p>

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="p-4">
                                        <form role="form" action="new_memo2.php" method="post">

                                            <div id="inputFormRow" class="form-group row">
                                                <div class="col-sm-3 mb-3 mb-sm-0">
                                                    <b><label for="week">Settimana</label></b>
                                                    <select id="week" name="week" class="form-control" required>
                                                        <option value="">Seleziona la settimana</option>
                                                        <option value="1">Settimana 1</option>
                                                        <option value="2">Settimana 2</option>
                                                        <option value="3">Settimana 3</option>
                                                        <option value="4">Settimana 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <?php
                                            include "./connect.php";

                                            $query = "SELECT f.ID, f.Nome 
                                                        FROM Farmaco AS f
                                                        JOIN Assunzione AS a
                                                        ON f.ID = a.Farmaco
                                                        JOIN Utente AS u
                                                        ON a.Utente = u.ID 
                                                        WHERE u.ID = $user[2]
                                                        ORDER BY f.Nome ASC";
                                            $result = mysqli_query($connessione, $query);
                                            ?>

                                            <div id="inputFormRow" class="form-group row">
                                                <div class="col-sm-3 mb-3 mb-sm-0">
                                                    <b><label for="medicine">Nome farmaco</label></b>
                                                    <select id="medicine" name="medicine[]" class="form-control" required>
                                                        <option value="">Seleziona il farmaco</option>
                                                        <?php
                                                        if (mysqli_num_rows($result) > 0) {
                                                            while ($row = mysqli_fetch_assoc($result)) {
                                                                echo '<option value="' . $row['ID'] . '">' . $row['Nome'] . '</option>';
                                                            }
                                                        } else {
                                                            echo '<option value=" ">Nessun farmaco presente</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3 mb-3 mb-sm-0">
                                                    <b><label for="day">Giorno</label></b>
                                                    <select id="day" name="day[]" class=" form-control" required>
                                                        <option value="">Seleziona il giorno</option>
                                                        <option value="1">Lunedì</option>
                                                        <option value="2">Martedì</option>
                                                        <option value="3">Mercoledì</option>
                                                        <option value="4">Giovedì</option>
                                                        <option value="5">Venerdì</option>
                                                        <option value="6">Sabato</option>
                                                        <option value="7">Domenica</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3 mb-3 mb-sm-0">
                                                    <b><label for="time">Ora</label></b>
                                                    <input type="time" id="time" name="time[]" class="form-control" autocomplete="off">
                                                </div>
                                                <div class="col-sm-2">
                                                    <div>
                                                        <b><label for="removeRow" style="color: #ffffff;">Nome farmaco</label></b>
                                                        <button id="removeRow" type="button" class="btn btn-danger btn-user">Elimina</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="newRow"></div>
                                            <a id="addRow" href="#"><b> + Aggiungi campo<b></a>

                                            <script type="text/javascript">
                                                // add row
                                                $("#addRow").click(function() {
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: 'medicine_list.php',
                                                        success: function(msg) {
                                                            info = JSON.parse(msg);
                                                            console.log(info);
                                                            var html = '';
                                                            html += '<div id="inputFormRow" class="form-group row">';
                                                            html += '<div class="col-sm-3 mb-3 mb-sm-0"><b><label for="medicine">Nome farmaco</label></b>';
                                                            html += '<select id="medicine" name="medicine[]" class="form-control" required >';
                                                            html += '<option value=" ">Seleziona il farmaco</option>';
                                                            if (info.length) {
                                                                for (var i = 0; i < info.length; i++) {
                                                                    html += '<option value="' + info[i][0] + '">' + info[i][1] + '</option>';
                                                                }
                                                            } else {
                                                                html += '<option value=" ">Nessun farmaco presente</option>';
                                                            }
                                                            html += '</select></div>';

                                                            html += '<div class="col-sm-3 mb-3 mb-sm-0"><b><label for="day">Giorno</label></b>';
                                                            html += '<select id="day" name="day[]" class=" form-control" required >';
                                                            html += '<option value="">Seleziona il giorno</option><option value="1">Lunedì</option><option value="2">Martedì</option><option value="3">Mercoledì</option><option value="4">Giovedì</option><option value="5">Venerdì</option><option value="6">Sabato</option><option value="7">Domenica</option>';
                                                            html += '</select></div>';
                                                            html += '<div class="col-sm-3 mb-3 mb-sm-0"><b><label for="time">Ora</label></b>';
                                                            html += '<input type="time" id="time" name="time[]" class="form-control" autocomplete="off"></div>';

                                                            html += '<div class="col-sm-2"><div><b><label for="removeRow" style="color: #ffffff;">Nome farmaco</label></b>';
                                                            html += '<button id="removeRow" type="button" class="btn btn-danger btn-user">Elimina</button></div>';
                                                            html += '</div></div>';

                                                            $('#newRow').append(html);
                                                        }
                                                    });
                                                });

                                                // remove row
                                                $(document).on('click', '#removeRow', function() {
                                                    $(this).closest('#inputFormRow').remove();
                                                });
                                            </script>
                                            <br><br><br><br><br><br>
                                            <button type="submit" class="btn btn-primary btn-user btn-block" name="submit">Aggiungi promemoria</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include "./footer.html" ?>
        </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <?php include "./logout.php" ?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>
</body>

</html>