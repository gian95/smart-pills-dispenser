/*
  Smart Pillbox

  An automatic smart pills dispenser that download the entire month schedule from a remote server
  and maintain all the information inside a queue. 
  
  The memo are stored in a chronological order from the closest to the last one. Each memo is 
  dequeued once at a time and a corresponding timer is set computing the difference between the current 
  time and the time instant at which the pill should be taken.

  When the timer expires a routine starts:
  - The information about the pill is displayed on the LCD display;
  - An acoustic alarm starts to warn the user;
  - The stepper motor starts to rotate until it reaches the correct position of the box inside which the pill is stored;
  - The alarm stops when the user push a button;
  - After this, a new node is dequeued from the queue;
  - The timer starts with the new value until the pill time arrives.
  
  Author: Gianluca Catalfamo
*/

//Wi-Fi connection and POST request to remote server library
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>

//Current time computation library
#include "time.h"

//Json parsing library
#include <ArduinoJson.h>

//LCD display library
#include <LiquidCrystal.h>

//Stepper motor library
#include <Stepper.h>

//Flash memory read/write library
#include <Preferences.h>

HTTPClient http;
WiFiMulti wifiMulti;

Preferences preferences;

//variables to configure and get time from an NTP server
const char* ntpServer = "pool.ntp.org";
// Set offset time in seconds to adjust for your timezone (GMT +1 = 3600)
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;

//passive buzzer sound
#define freq 2000
#define channel 0
#define resolution 8
//push button pin
#define button 13

//button to stop alert sound and debounce it
int buttonState = 1;         // current state of the button
int lastButtonState = 1;     // previous state of the button
long lastDebounceTime = 0;  // the last time the output pin was toggled
int debounceDelay = 50;     // the debounce time

//stepper motor 
const int stepsPerRevolution = 2048;
Stepper myStepper(stepsPerRevolution, 12, 27, 14, 26);
const int rolePerMinute = 15;         // Adjustable range of 28BYJ-48 stepper is 0~17 rpm
int one_step;
int current_box;                      //current position of the smart pillbox (stored in EEPROM)
int step_shift;                       //number of boxes between the current one and the next one of which to rotate 
int rotation;                         //rotation degree and direction

LiquidCrystal lcd(4, 16, 17, 5, 18, 19);

// Variables to save date and time
String dayStamp;
String timeStamp;
String anno;
String mese;
String giorno;
String ore;
String minuti;
String secondi;
char ntp_year[5];
char ntp_month[3];
char ntp_day[3];
char ntp_hour[3];
char ntp_minute[3];
char ntp_second[3];

int boxes;
int memo_number;
int starting_week_day;
int day_difference;

bool first_dequeue=true;
unsigned long startedWaiting;

//Variables to convert time from string to number of seconds and set the timer
char hour_medicine[3];
char minute_medicine[3];
int medicine_seconds;
int current_seconds;
long int timer;
int removeDay;

//Variables to save the value of each node after the parsing of the HTTP body response
String medicina;
char medicine[30];
int box;
int week;
int day;
String pill_time;
char pilltime[9];
 
//Last update of the local copy of the pillbox month schedule
String LastUpdate;
int update_schedule=2;  //updating rate for month schedule in minutes (max 30 minutes)
int update_status;

//interrupt for updating the month schedule
volatile int interruptUpdateCounter;
hw_timer_t * update_timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;
bool triggerAlert=false;

//structure of each node of the queue containing a memo
typedef struct memo{
  char medicine[30];
  int box;
  int week;
  int day; 
  char time[9];
  struct memo* next; 
}memo;

typedef struct queue{
  memo* head;
  memo* tail;
}queue;

queue *q;

memo extracted_nodo;



int isEmpty (struct queue* q){
  return (q->head==NULL) ? 1:0;
}

//function to put at the tail of the queue a new memo
void enqueue (struct queue* q, char medicine[], int box, int week, int day, char pilltime[]){ 
  memo* new_memo;
  new_memo = (memo*) malloc(sizeof(memo));
  if (new_memo == NULL){
    Serial.println("No available memory");
  }
  else{
    strcpy(new_memo->medicine, medicine);
    new_memo->box = box;
    new_memo->week = week;
    new_memo->day = day;
    strcpy(new_memo->time, pilltime);
    new_memo->next = NULL;
    if (isEmpty(q))
      q->head = new_memo;
    else
      q->tail->next = new_memo;
    q->tail = new_memo;
  }
}

void WaitingForMemo(){
  
  while(1){        
    currentTime();
    lcd.clear();
    lcd.setCursor(3, 0); lcd.print(giorno);
    lcd.setCursor(5, 0); lcd.print("-");
    lcd.setCursor(6, 0); lcd.print(mese);
    lcd.setCursor(8, 0); lcd.print("-");
    lcd.setCursor(9, 0); lcd.print(anno);
    lcd.setCursor(4, 1); lcd.print(timeStamp);
    
    if(interruptUpdateCounter>0){
   
      portENTER_CRITICAL(&timerMux);
      interruptUpdateCounter--;
      portEXIT_CRITICAL(&timerMux);
   
      Serial.println("Verifying month schedule update");
      DownloadMonthSchedule(&q);
      Serial.println("Display queue content:");
      display(q->head);
      
      //if the month schedule has been updated with new memo 
      //the first element is dequeued and the timer is computed again
      if(update_status==0 && memo_number>=1){
        return;
      }
    }
    delay(500);
  }
}

struct memo dequeue (struct queue *q){
  Serial.println("dequeue!");
  memo nodo;
  memo* current_memo;
  if (isEmpty(q)){
    Serial.println("No elements in the queue");
    WaitingForMemo();
  }

  strcpy( nodo.medicine, q->head->medicine);
  nodo.box = q->head->box;
  nodo.week = q->head->week;
  nodo.day = q->head->day;
  strcpy( nodo.time, q->head->time);
  current_memo = q->head;
  q->head = q->head->next;
  if (q->head == NULL){ /* if we had only one element */
    q->tail == NULL;
  }
  free(current_memo);
  return nodo;
}

void display(memo *head){
    if(head == NULL){
        Serial.println("-------------");
        Serial.println("NULL");
        Serial.println(" ");
    }
    else{
        Serial.println("-------------");
        Serial.print("Medicine:   "); Serial.println(head -> medicine);
        Serial.print("Box:    "); Serial.println(head -> box);
        Serial.print("Week:   "); Serial.println(head -> week);
        Serial.print("Day:    "); Serial.println(head -> day);
        Serial.print("Time:   "); Serial.println(head -> time);
        display(head->next);
    }
}
void currentTime(){
  
    struct tm timeinfo;
    while(!getLocalTime(&timeinfo)){
      Serial.println("Failed to obtain time");
    }

    strftime(ntp_year,5, "%Y", &timeinfo);
    strftime(ntp_month,3, "%m", &timeinfo);
    strftime(ntp_day,3, "%d", &timeinfo);
    strftime(ntp_hour,3, "%H", &timeinfo);
    strftime(ntp_minute,3, "%M", &timeinfo);
    strftime(ntp_second,3, "%S", &timeinfo);

   anno = String(ntp_year);
   mese = String(ntp_month);
   giorno = String(ntp_day);
   ore = String(ntp_hour);
   minuti = String(ntp_minute);
   secondi = String(ntp_second);
   dayStamp= anno + "-" + mese + "-" + giorno; 
   timeStamp= ore + ":" + minuti + ":" + secondi; 

}

int computeTimer(){

  extracted_nodo=dequeue(q);
  Serial.println("-------------");
  Serial.print("Medicine:   "); Serial.println(extracted_nodo.medicine);
  Serial.print("Box:    "); Serial.println(extracted_nodo.box);
  Serial.print("Week:   "); Serial.println(extracted_nodo.week);
  Serial.print("Day:    "); Serial.println(extracted_nodo.day);
  Serial.print("Time:   "); Serial.println(extracted_nodo.time);

  memset(hour_medicine, '\0', sizeof(hour_medicine));     //to clean the memory location
  strncpy(hour_medicine, extracted_nodo.time, 2);                   //copy the first two characters (hour)
  memset(minute_medicine, '\0', sizeof(minute_medicine)); //to clean the memory location
  strncpy(minute_medicine, extracted_nodo.time+3, 2);               //copy two characters strating from the fourth (minute)
  
  if(extracted_nodo.day==starting_week_day){removeDay=0;}
  else{removeDay=1;}
 
  medicine_seconds = ((extracted_nodo.week-1)*7+(((extracted_nodo.day-starting_week_day)+7)%7)-removeDay)*24*3600+atoi(hour_medicine)*3600 + atoi(minute_medicine)*60;

  currentTime();

  current_seconds = ((day_difference-1)%28)*24*3600+ore.toInt()*3600 + minuti.toInt()*60 + secondi.toInt();
  timer = medicine_seconds-current_seconds; 
  

  Serial.print("current_seconds: "); Serial.println(current_seconds);
  Serial.print("medicine_seconds: "); Serial.println(medicine_seconds);
  
  return timer;
}

void IRAM_ATTR onUpdateTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  interruptUpdateCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}
 
void DownloadMonthSchedule(struct queue** q){
  
  String MAC = WiFi.macAddress();
  currentTime();
  http.useHTTP10(true); //We use HTTP version 1.0 to use http.getStream() insted of http.getString()
  http.begin("http://192.168.1.13/download_schedule.php");                         
  http.addHeader("Content-Type", "application/json");
  String downloadRequest = "{\"MAC\":\""+MAC+"\",\"LastUpdate\":\""+LastUpdate+"\",\"Day\":\""+dayStamp+"\",\"Time\":\""+timeStamp+"\"}";
  int httpResponseCode = http.POST(downloadRequest);
  
  if(httpResponseCode>0){
    Serial.println(httpResponseCode);   //Print return code
    currentTime();
    LastUpdate = dayStamp+" "+timeStamp;
    Serial.print("Last download: ");  Serial.println(LastUpdate);
    
    // Parse response
    DynamicJsonDocument doc(2048);
    deserializeJson(doc, http.getStream()); 
     
    update_status=doc[0][0].as<int>();

    //updated with new memo
    if(update_status==0){

      //remove all the elements from the queue, to put again all the new schedule
      queue* puntatore=*q;
      puntatore-> head = NULL;
      puntatore->tail = NULL;
      
      boxes=doc[0][1].as<int>();
      memo_number=doc[1][0].as<int>();
      starting_week_day=doc[1][1].as<int>();
      day_difference=doc[1][2].as<int>();
      Serial.print("Memo: "); Serial.println(memo_number);
      Serial.print("Boxes: "); Serial.println(boxes);
      Serial.println(" ");
      
      // Read and enqueue values
      for(int i=2;i<memo_number+2;i++){
        medicina=doc[i][0].as<String>();
        box = doc[i][1].as<int>();
        week = doc[i][2].as<int>();
        day = doc[i][3].as<int>();
        pill_time = doc[i][4].as<String>();

        medicina.toCharArray(medicine,30);
        pill_time.toCharArray(pilltime,9);
        enqueue(*q, medicine, box, week, day, pilltime);
      }
    }
  }
  else{  
    Serial.print("Error on sending POST: ");
    Serial.println(httpResponseCode);
  }
  
  http.end();
}

void setup() {
 
  Serial.begin(115200);

  // add Wi-Fi networks you want to connect to, it connects strongest to weakest
  wifiMulti.addAP("TIM-24524711", "Nf2mKDQs16ivvr++@#Mf6nr()DnUaa");
  wifiMulti.addAP("Gianluca Honor 10", "d4021992ab72"); 
  //wifiMulti.addAP(ssid_3, password_3);

  //define the pin for the button as input digital pin
  pinMode(button, INPUT);
  
  //define pin for the PWM for the buzzer
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(2, channel);

  //define speed for the stepper motor
  myStepper.setSpeed(rolePerMinute);

  //create a namespace to store a variable on ESP32 EEPROM and define a key-value pair
  preferences.begin("Position", false); // false for read/write mode
  current_box = preferences.getInt("current_box", 1); //the initial default position is the box one
  Serial.printf("Current position (box): %u\n", current_box);
  preferences.end();
  
  //interrupt for updating the month schedule
  update_timer = timerBegin(0, 80, true); //false to count down
  timerAttachInterrupt(update_timer, &onUpdateTimer, true); //true so that the interruput generated is of edge type
  timerAlarmWrite(update_timer, update_schedule*60*1000000, true); //with true timer automatically reload
  timerAlarmEnable(update_timer); 
    
  // set up the LCD's number of columns and rows and print a message to the LCD.
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Smart pillbox");
  lcd.setCursor(0, 1);
  lcd.print("Benvenuto!");
  delay(3000);
  lcd.clear();
  
  // wait for WiFi connection
    while ((wifiMulti.run() != WL_CONNECTED)) {
    delay(2000);
    Serial.println("Connecting to WiFi..");
  }
 
  
  if ((wifiMulti.run() == WL_CONNECTED)) {

    //Configure the time with the settings defined earlier
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    
    Serial.println("\nConnected to " + WiFi.SSID() + " Use IP address: " + WiFi.localIP().toString()); // Report which SSID and IP is in use
    LastUpdate = "none";
    
    q = (queue*)malloc(sizeof(queue));
    q->head = NULL;
    q->tail = NULL;
    DownloadMonthSchedule(&q);
    Serial.println("Display queue content:");
    display(q->head);
  }

  //define the value of one step of the motor considering the number of boxes we have in the smart pillbox
  one_step=stepsPerRevolution/boxes;
}
  
void loop() {
  
  currentTime();
  lcd.clear();
  lcd.setCursor(3, 0); lcd.print(giorno);
  lcd.setCursor(5, 0); lcd.print("-");
  lcd.setCursor(6, 0); lcd.print(mese);
  lcd.setCursor(8, 0); lcd.print("-");
  lcd.setCursor(9, 0); lcd.print(anno);
  lcd.setCursor(4, 1); lcd.print(timeStamp);
  
  if(interruptUpdateCounter>0){
 
    portENTER_CRITICAL(&timerMux);
    interruptUpdateCounter--;
    portEXIT_CRITICAL(&timerMux);
 
    Serial.println("Verifying month schedule update");
    DownloadMonthSchedule(&q);
    Serial.println("Display queue content:");
    display(q->head);
    
    //if the month schedule has been updated with new memo 
    //the first element is dequeued and the timer is computed again
    if(update_status==0){
      
      timer=computeTimer();
      Serial.print("Timer: "); Serial.println(timer);
    
      while(timer<0){
        timer=computeTimer();
      }
      startedWaiting = millis();
    }
  }
    
  if(first_dequeue){
    timer=computeTimer();
    Serial.print("Timer: "); Serial.println(timer);
    
    while(timer<0){
      timer=computeTimer();
    }
    first_dequeue=false;
    startedWaiting = millis();
  }

  if((millis() - startedWaiting) >=(timer*1000)){
      triggerAlert=true;
  }

  if(triggerAlert){
    triggerAlert=false;
    lcd.clear();
    lcd.setCursor(3, 0); lcd.print("Take");
    lcd.setCursor(9, 0); lcd.print("your");
    lcd.setCursor(4, 1); lcd.print(extracted_nodo.medicine);

    preferences.begin("Position", false);
    current_box = preferences.getInt("current_box", 1);
    preferences.putInt("current_box", extracted_nodo.box);
    preferences.end();

    step_shift = abs(current_box - extracted_nodo.box);

    if(current_box > extracted_nodo.box){
      rotation = - (step_shift*one_step);  //rotate counterclockwise
    }
    else{
      rotation = step_shift*one_step;  //rotate clockwise
    }
    
    myStepper.step(rotation);
       
    //putting the duty cycle to 125 that is the 50% from 0 to 255 (the resolution is set equal to 8 bit) the max volume is obtained 
    ledcWrite(channel, 50);
    
    while(1){
      delay(300);
      ledcWriteTone(channel, 880);
      delay(300);
      ledcWriteTone(channel, 698);
      
      // read the pushbutton input pin:
      buttonState = digitalRead(button);
      //filter out any noise by setting a time buffer
      if ((millis() - lastDebounceTime) > debounceDelay) {
        // compare the buttonState to its previous state
        if (buttonState != lastButtonState) {
          if (buttonState == LOW) {
            // if the current state is LOW then the button has been pressed and the alert should stop
            //putting the duty cycle equal to zero I have volume zero
            ledcWrite(channel, 0);
            break;
          }
        } 
        // save the current state as the last state for next time through the loop
        lastButtonState = buttonState;
        lastDebounceTime = millis();
      }
    }
    
    timer=computeTimer();
    Serial.print("Timer: "); Serial.println(timer);
    
    while(timer<0){
      timer=computeTimer();
    }
    startedWaiting = millis();
  }
    
    delay(500);
    lcd.clear();
}
  
